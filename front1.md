# Prueba Frontend MyHotel

## Contexto

La intención de esta prueba es medir conocimientos 🤯. Por lo mismo, se recomienda demostrar conocimiento en múltiples aspectos (utilizar distintas herramientas).

Adicionalmente a esto, fijarse en calidad de código, que es de mucha importancia. Se recomienda para los estilos usar Angular Material y Tailwind/Bootstrap.

## Ejercicio

Para esta tarea, habrá que partir un proyecto en Angular 13+ desde cero (no importa la versión, siempre y cuando esté todo bien configurado a nivel de package.json). El proyecto debe tener un CRUD simple de un modelo a elegir (puede ser Foo si la imaginación está en sus días bajos). Este modelo, idealmente debe contar con algunos campos mínimos, como nombre, id, fecha de creación (Formato: 6-15-22, 9:03 AM) y algún arreglo (de nuevo, el arreglo puede ser de cualquier cosa, como colores favoritos). Además uno de los campos debe cambiar de color (verde, amarillo o rojo) según un criterio a elección por ustedes.
Por otra parte, se debe agregar algún tipo de búsqueda/ordenamiento (ya sea ordenar por columna o búsqueda por algún campo, idealmente dinámico).

Algunos aspectos más técnicos que debe considerar 🔧:
- Angular 13+ 🅰.
- Se deben utilizar reactive forms ⚛.
- No hay que ocupar base de datos ni backend (se puede, pero escapa del scope de este ejercicio), pero de no ocupar, esto no tiene que ser evidente desde el punto de vista del componente que utilice el servicio (hint 🕵️‍♀️: `rxjs/of`)
- Utilizar alguna directiva y/o pipe personalizado. Entiendo que puede no ser necesario para la envergadura del proyecto, pero el objetivo es demostrar conocimientos.
- Todo tiene que estar en inglés (variables, comentarios)
- Agregar un `README.md` básico para correr la aplicación. Da lo mismo el idioma en el que esté este README (inglés o español eso sí). De haber cosas que necesiten explicación, comentar, etc., también utilizar este archivo (por qué se hizo algo de la manera `x`? Aspectos interesantes que no son tan evidentes).
- Suma puntos si se implementa NGXS, aunque sea algo básico 🙄.
- La prueba se debe entregar en un repositorio público.

## Preguntas
CON TODA CONFIANZA mandar mails con dudas a anibal@myhotel.cl y/o hardy@myhotel.cl. La idea es no quedarse con las dudas 😁.
